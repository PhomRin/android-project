package com.example.adapter.recyclerview;

public class School {
    private int imageschool;
    private String name;
    private String phone;
    private String website;
    private String email;
    private String location;

    public School(int imageschool, String name, String phone, String website, String email, String location) {
        this.imageschool = imageschool;
        this.name = name;
        this.phone = phone;
        this.website = website;
        this.email = email;
        this.location = location;
    }

    public int getImageschool() {
        return imageschool;
    }

    public void setImageschool(int imageschool) {
        this.imageschool = imageschool;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
