package com.example.adapter.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.adapter.R;

import java.util.ArrayList;

public class RecyclerViewMainActivity extends AppCompatActivity {

    private ArrayList<School> schools = new ArrayList<>();
    private RecyclerView recyclerView;
    private SchoolAdapter schoolAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_main);

        recyclerView = findViewById(R.id.my_recycler_view);

        schools.add(new School(R.drawable.kshrd,"Korean Software HRD","012 345 678","www.kshrd.com.kh","info@kshrd.edu.kh","st007"));
        schools.add(new School(R.drawable.rupp,"Royal University of Phnom Penh","012 987 654","www.rupp.com.kh","info@rupp.edu.kh","st001"));
        schools.add(new School(R.drawable.puc,"Pannasastra International School","011 223 334","www.puc.com.kh","info@puc.edu.kh","st002"));
        schools.add(new School(R.drawable.acleda,"Acleda university","012 345 678","www.acleda.com.kh","info@acleda.edu.kh","st003"));
        schools.add(new School(R.drawable.itc,"Institute of technology of cambodia","012 345 678","www.itc.com.kh","info@itc.edu.kh","st004"));

        schoolAdapter = new SchoolAdapter(schools);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                RecyclerView.VERTICAL,false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(schoolAdapter);
    }
}
