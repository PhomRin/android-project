package com.example.adapter.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.adapter.R;

import java.util.ArrayList;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.MySchoolViewHolder> {


    private ArrayList<School> schools = new ArrayList<>();

    public SchoolAdapter(ArrayList<School> schools){
        this.schools = schools;
    }

    @NonNull
    @Override
    public MySchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.school_layout,parent, false);

        return new MySchoolViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MySchoolViewHolder holder, int position) {
        holder.imageBig.setImageResource(schools.get(position).getImageschool());
        holder.imageSmall.setImageResource(schools.get(position).getImageschool());
        holder.textName.setText(schools.get(position).getName());
        holder.textPhone.setText(schools.get(position).getPhone());
        holder.textWebsite.setText(schools.get(position).getWebsite());
        holder.textEmail.setText(schools.get(position).getEmail());
        holder.textLocation.setText(schools.get(position).getLocation());
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }

    public class MySchoolViewHolder extends RecyclerView.ViewHolder{
        ImageView imageBig, imageSmall;
        TextView textName, textPhone, textWebsite, textEmail, textLocation;
        public MySchoolViewHolder(@NonNull View itemView) {
            super(itemView);
            imageBig = itemView.findViewById(R.id.image_school);
            imageSmall = itemView.findViewById(R.id.image_small);
            textName = itemView.findViewById(R.id.text_name);
            textPhone = itemView.findViewById(R.id.text_phone);
            textWebsite = itemView.findViewById(R.id.text_website);
            textEmail = itemView.findViewById(R.id.text_email);
            textLocation = itemView.findViewById(R.id.text_location);
        }
    }
}


