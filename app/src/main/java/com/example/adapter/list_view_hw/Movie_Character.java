package com.example.adapter.list_view_hw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.adapter.Adapter_Listview;
import com.example.adapter.R;

public class Movie_Character extends AppCompatActivity {

    int[] pictures = {R.drawable.robert_downey_jr,R.drawable.chris_evans,
                     R.drawable.chris_hemsworth,R.drawable.scarlett_johansson,
                     R.drawable.jeremy_renner,R.drawable.mark_ruffalo};

    String[] name_actor = {"Robert Downey JR","Chris Evans","Chris Hemsworth",
                            "Scarlett Johansson","Jeremy Renner","Mark Ruffalo"};

    String[] name_charac = {"Iron Man","Captain America","Thor","Black Widow","HawkEyes","Hulk"};

    String[] sex = {"Male","Male","Male","Female","Male","Male"};

    String[] year = {"54","38","36","34","48","51"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter__listview);

        final ListView mlvPicture = findViewById(R.id.list_view_item);

        final MovieCharacterCustom movieCharacterCustom = new MovieCharacterCustom(this,
                pictures,name_actor,name_charac,sex,year);
            mlvPicture.setAdapter(movieCharacterCustom);

            mlvPicture.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Object pictures = movieCharacterCustom.getPicture(position);
                    Object name_actor = movieCharacterCustom.getItem(position);
                    Object name_charac = movieCharacterCustom.getName_Charac(position);
                    Object sex = movieCharacterCustom.getSex(position);
                    Object year = movieCharacterCustom.getyear(position);

                    Intent intent = new Intent(Movie_Character.this,Info_Character.class);
                    intent.putExtra("Name",name_actor.toString());
                    intent.putExtra("Picture",(Integer)pictures);
                    intent.putExtra("Sex",sex.toString());
                    intent.putExtra("Charac",name_charac.toString());
                    intent.putExtra("Year",year.toString());
                    startActivity(intent);
                }
            });
    }
}
