package com.example.adapter.list_view_hw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.adapter.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class Info_Character extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info__character);

        final CircleImageView mCIpicture= findViewById(R.id.cicrle_picture);
        final TextView mTvActorName = findViewById(R.id.tv_actor_name);
        final Button mBtback =findViewById(R.id.bt_back);
        final TextView mTvSex = findViewById(R.id.text_sex);
        final TextView mTvYear = findViewById(R.id.text_year);
        final TextView mTvCharac= findViewById(R.id.text_Charac);

        Bundle extra = getIntent().getExtras();

        String actorname = extra.getString("Name");
        Integer actorpic = extra.getInt("Picture");
        String actorsex = extra.getString("Sex");
        String actorcharac = extra.getString("Charac");
        String actoryear = extra.getString("Year");

        mCIpicture.setImageResource(actorpic);
        mTvActorName.setText(actorname);
        mTvSex.setText(actorsex);
        mTvYear.setText(actoryear);
        mTvCharac.setText(actorcharac);

        mBtback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = getIntent();
                setResult(RESULT_OK,backIntent);
                finish();
            }
        });
    }
}
