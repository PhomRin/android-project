package com.example.adapter.list_view_hw;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.adapter.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class MovieCharacterCustom extends BaseAdapter {

    private int[] pictures;
    private String[] name_actor;
    private String[] name_charac;
    private String[] sex;
    private String[] year;
    private Context context;


    public MovieCharacterCustom(Context context,int[] pictures, String[] name_actor,
                                String[] name_charac, String[] sex,String[] year) {
        this.context=context;
        this.pictures=pictures;
        this.name_actor=name_actor;
        this.name_charac=name_charac;
        this.sex=sex;
        this.year=year;

    }

    @Override
    public int getCount() {
        return name_actor.length;
    }

    @Override
    public Object getItem(int position) {
        return name_actor[position];
    }

    public Object getPicture (int position){
        return pictures[position];
    }

    public Object getName_Charac (int position){
        return name_charac[position];
    }

    public Object getSex (int position){
        return sex[position];
    }

    public Object getyear (int position){
        return year[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = LayoutInflater.from(context).inflate(R.layout.activity_movie_character,null);
        CircleImageView circleImagePicture = convertView.findViewById(R.id.character_profile_image);
        TextView textName_Actor = convertView.findViewById(R.id.tv_actor);
        TextView textName_Charac = convertView.findViewById(R.id.tv_character);

        circleImagePicture.setImageResource(pictures[position]);
        textName_Actor.setText(name_actor[position]);
        textName_Charac.setText(name_charac[position]);

        return convertView;
    }
}
