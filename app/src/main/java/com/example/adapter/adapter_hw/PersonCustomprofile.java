package com.example.adapter.adapter_hw;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.adapter.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class PersonCustomprofile extends BaseAdapter {

    private int[] photo;
    private String[] name;
    private String[] job;
    private Context context;

    public PersonCustomprofile (Context context, int[] photo,String[] name, String[] job){

        this.context=context;
        this.photo=photo;
        this.name=name;
        this.job=job;
    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return name[position];
    }

    public Object getPhoto (int position){
        return photo[position];
    }

    public Object getJob (int position){

        return job[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = LayoutInflater.from(context).inflate(R.layout.activity_person_profile,null);
        CircleImageView circleImagePhoto = convertView.findViewById(R.id.person_profile_image);
        TextView textName = convertView.findViewById(R.id.tv_person_name);
        TextView textJob = convertView.findViewById(R.id.tv_job);

        circleImagePhoto.setImageResource(photo[position]);
        textName.setText(name[position]);
        textJob.setText(job[position]);

        return convertView;
    }

}
