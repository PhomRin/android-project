
package com.example.adapter.adapter_hw;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.adapter.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class Person_profile extends AppCompatActivity {

    int[] photo = {R.drawable.photo1, R.drawable.photo2, R.drawable.photo3, R.drawable.photo4,
                   R.drawable.photo5, R.drawable.photo6, R.drawable.photo7, R.drawable.photo8,
                   R.drawable.photo9 };

    String[] name = {"Saksith", "Minea", "Moona", "Mony", "Hout", "Kethya", "Kannitha",
                      "Panha", "NiNi"};

    String[] job = {"Web Designer", "Teacher", "Lawyer", "Business Man", "Programmer",
                    "Police Man","Business Woman", "Manager", "CEO"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter__listview);

        final ListView mlvPhoto = findViewById(R.id.list_view_item);

        final PersonCustomprofile personCustomprofile = new PersonCustomprofile(this,photo,name,job);
        mlvPhoto.setAdapter(personCustomprofile);

        mlvPhoto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object photo = personCustomprofile.getPhoto(position);
                Object name = personCustomprofile.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(Person_profile.this);

                View view1 = getLayoutInflater().inflate(R.layout.person_profile_dialog,null);
                builder.setView(view1);

                final CircleImageView circleImageView =view1.findViewById(R.id.circle_image_person_photo);
                final TextView mTvName = view1.findViewById(R.id.tv_person_name_dialog);

                circleImageView.setImageResource((Integer)photo);
                mTvName.setText(name.toString());

                builder.create().show();

            }
        });

    }
}
