package com.example.adapter.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.adapter.R;

public class ListView_MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview_main);

        String[] emails = new String[50];

        final ListView listView = findViewById(R.id.my_list_view);

        for (int i=0; i<50; i++ )
        {
            emails[i]="Number" + i + "@gmail.com";
        }

        ArrayAdapter<String> emailAdapter = new ArrayAdapter<>(this,
                R.layout.list_view_item_layout,R.id.text_list_view,emails);

        listView.setAdapter(emailAdapter);
    }

}
