package com.example.adapter.gridview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.adapter.R;
import com.example.adapter.model.UserModel;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileCustomAdapter extends BaseAdapter {

    //Declaration:
    private TextView mName;
    private CircleImageView mProfile;
    private Context mContext;

    private List<UserModel> mUser = new ArrayList<>();

    public UserProfileCustomAdapter(Context mContext, List<UserModel> mUser) {
        this.mContext = mContext;
        this.mUser = mUser;
    }

    @Override
    public int getCount() {
        return mUser.size();
    }

    @Override
    public Object getItem(int position) {
        return mUser.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(mContext)
                .inflate(R.layout.user_grid_layout,null);

        this.initializeWidgets(convertView);
        this.initializeWidgetValue(mUser.get(position));
        return convertView;
    }

    private void initializeWidgets(View parent){
        this.mName = parent.findViewById(R.id.text_grid_name);
        this.mProfile = parent.findViewById(R.id.im_profile);
    }

    private void initializeWidgetValue(UserModel user){
        this.mName.setText(user.getName());
        this.mProfile.setImageResource(R.drawable.chris_evans);

    }
}
