package com.example.adapter.gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import com.example.adapter.R;
import com.example.adapter.model.UserModel;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;

public class GridViewMainActivity extends AppCompatActivity {

    private GridView mMyGridView;
    private List<UserModel> mUser = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_main);

        mMyGridView = findViewById(R.id.my_grid_view);

        Faker faker = new Faker();
        for(int i=0; i<50; i++){
            String name = faker.name().fullName();
            String profile = faker.avatar().image();
            this.mUser.add(new UserModel(name,profile));
        }
        UserProfileCustomAdapter userProfileCustomAdapter =
                new UserProfileCustomAdapter(this,this.mUser);
        mMyGridView.setAdapter(userProfileCustomAdapter);
    }

}
