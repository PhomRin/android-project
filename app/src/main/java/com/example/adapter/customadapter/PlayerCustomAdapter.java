package com.example.adapter.customadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adapter.Adapter_Listview;
import com.example.adapter.R;

public class PlayerCustomAdapter extends BaseAdapter {

    private  int[] profile;
    private String[] title;
    private Context context;

    public PlayerCustomAdapter(Context context,int[] profile, String[] title){

        this.context=context;
        this.profile=profile;
        this.title=title;
    }


    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public Object getItem(int position) {
        return title[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = LayoutInflater.from(context).inflate(R.layout.activity_player__profile,null);
        ImageView imageProfile = convertView.findViewById(R.id.imageView_player);
        TextView textTitle = convertView.findViewById(R.id.textView_player);

        imageProfile.setImageResource(profile[position]);
        textTitle.setText(title[position]);

        return convertView;
    }
}
