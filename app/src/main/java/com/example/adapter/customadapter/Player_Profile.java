package com.example.adapter.customadapter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

import com.example.adapter.R;

public class Player_Profile extends AppCompatActivity {

    int[] profile = {R.drawable.facebook, R.drawable.instagram, R.drawable.facebook};
    String[] title = {"Facebook", "Instagram", "Line"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter__listview);

        final ListView mlvProfile = findViewById(R.id.list_view_item);

        PlayerCustomAdapter playerCustomAdapter = new PlayerCustomAdapter(this,
                profile,title);
        mlvProfile.setAdapter(playerCustomAdapter);
    }
}
