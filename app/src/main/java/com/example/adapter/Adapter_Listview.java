package com.example.adapter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Adapter_Listview extends AppCompatActivity {

    String[] players = new String[50];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter__listview);


        final ListView listViewPlayer = findViewById(R.id.list_view_item);

        for (int i=0; i<50; i++){
            players[i] = "Player-" + (i+1);
        }

        ArrayAdapter<String> playersAdapter = new ArrayAdapter<>(Adapter_Listview.this,
                R.layout.players_text_view,R.id.text_player,players);

        listViewPlayer.setAdapter(playersAdapter);
    }
}
